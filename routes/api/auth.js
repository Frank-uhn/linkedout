const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const auth = require('../../middleware/auth');
const jwt = require('jsonwebtoken');
const config = require('../../config/default.json');
const { check, validationResult } = require('express-validator');
const User = require('../../models/Users');
//@route    GET /
//desc      Test routes
//@access   Public

// router.get('/',(req,res)=>{
//   res.send('Auth route')
// })


// @route    GET /auth
// @desc     Get user by token
// @access   Private
router.get('/', /*auth,*/ async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select('-password');
    res.json(user);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

//@route    POST /auth
//desc      Authen & get token
//@access   Public
router.post('/',
  [
    //check('name','Cần điền đủ tên').notEmpty(),
    check('email','Vui lòng điền email').isEmail(),
    check('password','Vui lòng điền mật khẩu').exists()
  ] 
,
async (req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()){
    return res.status(400).json({errors: errors.array()})
  }
  const {name, email, password} = req.body;
  
  try{
    //Tim  user trong DB co hay chua
    let user = await User.findOne({email });
    if(!user){
      return res.status(400)
                .json({errors:[{msg:'Người dùng không tồn tại'}]});
    }
    //so mk
    const isMatch = await bcrypt.compare(password, user.password);

      if (!isMatch) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Vui lòng kiểm tra lại mật khẩu hoặc email' }] });
      }
    
    //res.send('User route')
    const payload = {
      user: {
        id: user.id
      }
    };

    jwt.sign(
      payload,
      config.jwtSecret,
      {expiresIn: "2 days"},
      (err,token)=>{
        if(err) throw err;
        res.json({token})
      });

  }catch(err){
    console.error(err.message);
    res.status(500).send('Code sai');
  }

})
module.exports = router;


