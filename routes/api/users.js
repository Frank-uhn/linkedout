const express = require('express');
const { check, validationResult } = require('express-validator');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../../models/Users');
const config = require('../../config/default.json');

//@route    GET api/routes
//desc      Test routes
//@access   Public

router.get('/',(req,res)=>{
  res.send('User route');
})



//@route    POST api/routes
//desc      Register user 
//@access   Public
router.post('/',
  [
    check('name','Cần điền đủ tên').notEmpty(),
    check('email','Cần điền đủ email').isEmail(),
    check('password','Mật khẩu ít nhất 6 ký tự').isLength({min:6})
  ]
,
async (req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()){
    return res.status(400).json({errors: errors.array()})
  }
  const {name, email, password} = req.body;
  
  try{
    //Tim  user trong DB co hay chua
    let user = await User.findOne({email });
    if(user){
      return res.status(400).json({errors:[{msg:'Người dùng đã đăng ký'}]});
    }
    //Them cai avatar bang gravatar
    const avatar = gravatar.url(email,{
      s:'200',
      r:'pg',
      d:'mm'
    })
    //tao cai object moi
    user = new User({
      name,
      email,
      avatar,
      password
    });
    // hash
    const salt = await bcrypt.genSalt(10);

    user.password = await bcrypt.hash(password, salt);
    
    await user.save();
    //res.send('User route')
    const payload = {
      user: {
        id: user.id
      }
    };
    jwt.sign(
      payload,
      config.jwtSecret,
      {expiresIn: "2 days"},
      (err,token)=>{
        if(err) throw err;
        res.json({token})
      });

  }catch(err){
    console.error(err.message);
    res.status(500).send('Code sai');
  }

})

module.exports = router;