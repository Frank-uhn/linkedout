const jwt = require('jsonwebtoken');

const config = require('config');

const auth = (req,res,next)=>{
  //lay token tu header
  const token = req.header('x-auth-token');

  if(!token){
    return res.status(401)
              .json({msg:'Khum co token, quyen truy cap that bai'})
  }

  try{
    jwt.verify(token, config.get('jwtSecret'), (error, decoded) => {
      if (error) {
        return res.status(401).json({ msg: 'Token không hợp lệ' });
      } else {
        req.user = decoded.user;
        next();
      }
    });

  }catch(err){
    console.log('Viet code sai trong middleware auth')
    res.status(500).json({msg:'Loi he thong'});
  }

}
module.exports = auth;