const express = require('express');
const connectDB = require('./config/db');
const bodyParser = require('body-parser')
const app = express();

const userApi = require('./routes/api/users');
const authApi = require('./routes/api/auth');
const profileApi = require('./routes/api/profile');
const postApi = require('./routes/api/posts');



app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//Connect DB

connectDB();
app.get('/',(req,res)=>{
  res.send('Api is running');
})
//Define API
app.use('/api/users', userApi);
app.use('/api/auth', authApi);
app.use('/api/profile', profileApi);
app.use('/api/posts', postApi);


const PORT = process.env.PORT || 5000;


app.listen(PORT,()=>{
  console.log(`server stated on port ${PORT}`);
})